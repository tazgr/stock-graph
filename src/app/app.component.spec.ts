import { async, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ModalModule } from 'ngx-bootstrap';
import { ApiService } from './services/api.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { sampleGoogleData } from './services/api.service.spec';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ModalModule.forRoot(),
        ReactiveFormsModule,
      ],
      providers: [
        ApiService,
      ],
      declarations: [
        AppComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const appComponent = fixture.componentInstance;
    expect(appComponent).toBeTruthy();
  });

  it('should render title, form and button', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.jumbotron').textContent).toContain('Stock Graph');
    expect(compiled.querySelector('form')).toBeTruthy();
    expect(compiled.querySelector('.btn').textContent).toContain('Open Modal');
  });

  it('Should accept valid data in the form', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const appComponent = fixture.componentInstance;
    fixture.detectChanges();
    await fixture.whenStable();

    appComponent.form.controls.symbol.setValue('GOOG');
    appComponent.form.controls.startDate.setValue('2017-01-01');
    appComponent.form.controls.endDate.setValue('2017-03-06');
    appComponent.form.controls.email.setValue('test@test.com');
    expect(appComponent.form.valid).toBeTruthy();

    appComponent.form.controls.symbol.setValue('ampla');
    appComponent.form.controls.startDate.setValue('2017-01-01');
    appComponent.form.controls.endDate.setValue('2017-03-06T00:17:00');
    appComponent.form.controls.email.setValue('');
    expect(appComponent.form.invalid).toBeTruthy();
  });

  // noinspection DuplicatedCode
  it('Should call the service when button is pressed', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const apiService = TestBed.inject(ApiService);
    const appComponent = fixture.componentInstance;
    fixture.detectChanges();
    await fixture.whenStable();
    appComponent.form.controls.symbol.setValue('test');
    appComponent.form.controls.startDate.setValue('2017-01-01');
    appComponent.form.controls.endDate.setValue('2017-03-06');
    appComponent.form.controls.email.setValue('test@test.com');

    spyOn(apiService, 'getSymbolData').and.returnValue(of(sampleGoogleData.dataset));
    appComponent.openModal();
    await fixture.whenStable();
    expect(appComponent.results.data).toBeTruthy();
  });

  // noinspection DuplicatedCode
  it('Should render the chart', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const apiService = TestBed.inject(ApiService);
    const appComponent = fixture.componentInstance;
    fixture.detectChanges();
    await fixture.whenStable();
    appComponent.form.controls.symbol.setValue('test');
    appComponent.form.controls.startDate.setValue('2017-01-01');
    appComponent.form.controls.endDate.setValue('2017-03-06');
    appComponent.form.controls.email.setValue('test@test.com');

    spyOn(apiService, 'getSymbolData').and.returnValue(of(sampleGoogleData.dataset));
    appComponent.openModal();
    await fixture.whenStable();
    expect(appComponent.modalTemplate.elementRef.nativeElement.textContent).toBeTruthy();
  });
});
