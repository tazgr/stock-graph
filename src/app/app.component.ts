import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ApiService, Dataset } from './services/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('modalTemplate')
  public modalTemplate: TemplateRef<HTMLDivElement>;
  private readonly datePattern: RegExp = /\d{4}-\d{1,2}-\d{1,2}/;
  public form: FormGroup;
  public results: Dataset;
  public modalRef: BsModalRef;

  constructor(private modalSrv: BsModalService, private apiSrv: ApiService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      symbol: new FormControl('GOOG', [Validators.required]),
      startDate: new FormControl('2017-01-01', [Validators.required, Validators.pattern(this.datePattern)]),
      endDate: new FormControl('2017-03-06', [Validators.required, Validators.pattern(this.datePattern)]),
      email: new FormControl('a@a.com', [Validators.required, Validators.email])
    });
  }

  openModal() {
    this.apiSrv.getSymbolData(this.form.value.symbol, this.form.value.startDate, this.form.value.endDate)
      .subscribe(res => {
        this.results = res;
        this.modalRef = this.modalSrv.show(this.modalTemplate, {animated: true, focus: true});
      }, err => {
        console.error(err);
        this.form.reset();
      });
  }

  getMailLink() {
    return `mailto:${this.form.value.email}?subject=${this.results.name.replace(' ', '%20')}&body=From%20${this.form.value.startDate}%20to%20${this.form.value.endDate}`;
  }
}
