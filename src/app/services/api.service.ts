import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public readonly url: string = 'https://www.quandl.com/api/v3/datasets/WIKI';

  constructor(private http: HttpClient) { }

  getSymbolData(symbolName: string, from: string, to: string): Observable<Dataset> {
    const params = new HttpParams({
      fromObject: {
        order: 'asc',
        start_date: from,
        end_date: to,
      }
    });
    return this.http.get<Response>(`${this.url}/${symbolName}.json`, {params})
      .pipe(
        retry(3),
        map(resp => {
          resp.dataset.data = resp.dataset.data.map(data => {
            const openPos = resp.dataset.column_names.indexOf('Open');
            const closePos = resp.dataset.column_names.indexOf('Close');
            const datePos = resp.dataset.column_names.indexOf('Date');
            return [data[datePos], data[openPos], data[openPos], data[closePos], data[closePos]];
          });
          return resp.dataset;
        })
      );
  }
}

// region Response
export interface Response {
  dataset: Dataset;
}

export type TimeSeries = Array<Array<string | number>>;

export interface Dataset {
  id: number;
  dataset_code: string;
  database_code: string;
  name: string;
  description: string;
  refreshed_at: string;
  newest_available_date: string;
  oldest_available_date: string;
  column_names: string[];
  frequency: string;
  type: string;
  premium: boolean;
  limit: null;
  transform: null;
  column_index: null;
  start_date: string;
  end_date: string;
  data: TimeSeries; // look for column_names
  collapse: null;
  order: string;
  database_id: number;
}

// endregion
